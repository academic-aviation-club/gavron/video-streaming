import sys
import cv2
import imagezmq


def loop(server):
    # Create a hub for receiving images from cameras
    image_hub = imagezmq.ImageHub()

    # Start main loop
    while True:
        machine_id, image = image_hub.recv_image()
        print("Got image")
        if machine_id in server.active_flights:
            # Doesn't matter because in PUB/SUB configuration, there is no callback
            image_hub.send_reply(b'OK')
            print("OK")
        else:
            # Doesn't matter because in PUB/SUB configuration, there is no callback
            image_hub.send_reply(b'')
            print("")

        server.saveImage(machine_id, image)
