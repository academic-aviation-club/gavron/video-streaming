cheroot==8.4.5
CherryPy==18.6.0
imagezmq==1.1.1
jaraco.classes==3.1.0
jaraco.collections==3.0.0
jaraco.functools==3.0.1
jaraco.text==3.2.0
more-itertools==8.6.0
numpy==1.19.4
opencv-python==4.4.0.46
portend==2.6
pytz==2020.4
pyzmq==19.0.2
six==1.15.0
tempora==4.0.1
zc.lockfile==2.0
