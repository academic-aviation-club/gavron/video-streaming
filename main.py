import cherrypy
import api_server
import img_server
import threading

cherrypy.server.socket_host = '0.0.0.0'

server = api_server.ApiServer()
x = threading.Thread(target=img_server.loop,args={server})

x.start()
cherrypy.quickstart(server)

