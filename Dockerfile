FROM wnt3rmute/archlinux-with-goodies

RUN pacman -Syyu --noconfirm mesa

RUN mkdir /video_streaming
COPY requirements.txt /video_streaming
RUN pip install -r  /video_streaming/requirements.txt

COPY . /video_streaming
WORKDIR /video_streaming
