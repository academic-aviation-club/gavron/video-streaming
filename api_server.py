import cherrypy
import json
import os
import os.path
import cv2
from cherrypy.lib import static
import glob

localDir = os.path.dirname(__file__)
absDir = os.path.join(os.getcwd(), localDir)


def json_response(status_code, comment=None, content=None):
    j = {"status": status_code, "comment": comment, "content": content}

    return json.dumps(j)


class ApiServer(object):
    active_flights = {"test": "test"}

    def saveImage(self, machine_id, image):
        flight_id = self.active_flights[machine_id]
        filesnum = len(glob.glob(absDir + '/flights/' + flight_id + '/*.jpg'))
        print("Image saved to:"+'{0}/flights/{1}/{2}.jpg'.format(absDir, flight_id, filesnum))
        cv2.imwrite('{0}/flights/{1}/{2}.jpg'.format(absDir, flight_id, filesnum), image)

    @cherrypy.expose
    def getLastImage(self, flight_id):
        filesnum = len(glob.glob(absDir + '/flights/' + flight_id + '/*.jpg'))
        path = os.path.join(absDir + '/flights/' + flight_id + '/', str(filesnum-1)+ '.jpg')
        return static.serve_file(path, 'application/x-download',
                                 'attachment', os.path.basename(path))
    @cherrypy.expose
    def getImage(self, flight_id, image_id):
        path = os.path.join(absDir + '/flights/' + flight_id + '/', image_id + '.jpg')
        return static.serve_file(path, 'application/x-download',
                                 'attachment', os.path.basename(path))
    @cherrypy.expose
    def index(self):
        return open(absDir + "/viewer.html")
    @cherrypy.expose
    def getFlightInfo(self, flight_id):
        if not os.path.exists(absDir + '/flights/' + flight_id + '/'):
            return json_response(400, "There is no flight with given id")
        else:
            return json_response(200, content=len(glob.glob(absDir + '/flights/' + flight_id + '/*.jpg')))
        return

    @cherrypy.expose
    def getActiveFlights(self):
        return json_response(200, "", self.active_flights)

    @cherrypy.expose
    def deleteFlight(self, machine_id, flight_id):
        del self.active_flights[machine_id]
        return json_response(200)

    @cherrypy.expose
    def putFlight(self, machine_id, flight_id):
        if machine_id in self.active_flights:
            return json_response(400, "Drone is in flight :" + str(self.active_flights[machine_id]))

        self.active_flights[machine_id] = flight_id
        if not os.path.exists(absDir + '/flights/' + flight_id + '/'):
            os.mkdir(absDir + '/flights/' + flight_id + '/')

        return json_response(200)
