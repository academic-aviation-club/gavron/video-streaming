import socket
import time
import imagezmq
import cv2

vidcap = cv2.VideoCapture('video.mp4')

sender = imagezmq.ImageSender(connect_to='tcp://127.0.0.1:5555')

rpi_name = socket.gethostname()  # send RPi hostname with each image
success, image = vidcap.read()
count = 0
while count < 600:  # send images as stream until Ctrl-C
    vidcap.read()  # save frame as JPEG file
    if count % 80 == 0:
        success, image = vidcap.read()
        print('Sent a new frame: ', success)
        sender.send_image("test", image)
    count += 1
